from pathlib import Path
from cartopy import crs
import cftime
import cartopy
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytz

matplotlib.use("Agg")


__all__ = ("get_filenames", "_pool_wrapper", "Plotter", "plot_map")


def get_filenames(fname, start, end):
    """Get only filenames within certain dates.

    This function tries to guess the dates within a netcdf file by the
    date string as suffix e.g. '19990101T0000-19990101T1200' and return
    either the filename (if the constructed dates are within the given range,
    by start and end time). If start and/or end are None then no date
    comparison is done and the filename is returned. If the code doesn't find
    a date string in the filename the filename also gets returned.

    The usage of the function is to minimize the number of netcdf files
    that have to be opened.

    Parameters:
    ===========
        fname (str, pathlib.Path):
            Input filename
        start (datetime):
            First timestep of the considered time period
        end (datetime):
            Last timestep of the considered time period

    Returns:
    ========
    fname (str, pathlib.Path)
    """
    if isinstance(start, str) and start:
        try:
            start = pd.Timestamp(start)
        except Exception:
            start = None
    if isinstance(end, str) and end:
        try:
            end = pd.Timestamp(end)
        except Exception:
            end = None
    try:
        start = start.year
    except AttributeError:
        start = start or -9999
    try:
        end = end.year
    except AttributeError:
        end = end or 100000
    try:
        fname2 = Path(fname).with_suffix("").name
    except ValueError:
        return
    if not str(fname).strip():
        return
    try:
        ts = pd.DatetimeIndex(str(fname2).split("_")[-1].strip().split("-"))
    except Exception:
        return str(fname).strip()
    if ts[0].year >= start and ts[-1].year <= end and str(fname).strip():
        return str(fname).strip()


class MapCreator:
    """Base Clase that servers as a map factory."""

    def __init__(self, **kwargs):
        """Instanciated a map class with the most important attributes."""

        for key, value in (
            ("color", "black"),
            ("resolution", "10m"),
            ("linewidth", 0.8),
            ("transform", crs.PlateCarree()),
        ):
            kwargs.setdefault(key, value)
        self.color = kwargs.pop("color", "black")
        self.res = kwargs.pop("resolution", "10m")
        self.linewidth = kwargs.pop("linewidth", 0.8)
        self.transform = kwargs.pop("transform")
        self.figsize = kwargs.pop("figsize", (9, 9))
        self.extent = kwargs.pop("lonlatbox", None)
        self._kwargs = kwargs
        self.fig = plt.figure(figsize=self.figsize)
        self.fig.subplots_adjust(
            left=0.01, right=0.99, hspace=0, wspace=0, top=1, bottom=0.03
        )


class GlobalMap(MapCreator):
    """Class for Map objects on a Global Map."""

    def __init__(self, lon, lat, proj, **kwargs):
        """Instanciate a global map using Cartopy.

        Parameters:
        ==========
            lon (xr.DataArray, np.array):
                the Longitude Vector
            lat (xr.DataArray, np.array):
                the Latitude Vector
            proj (str, cartopy.crs):
                The cartopy map projection, if proj is of type str then
                the projection is going to be created using the evaluation
                proj string and the cartopy.crs call
            kwargs:
                Additional keyword arguments that are passed to the
                map creator factory
        """

        super().__init__(**kwargs)
        if isinstance(proj, str):
            proj = eval(f"crs.{proj}")
        self.proj = proj
        self.lon = lon
        self.lat = lat
        self.ax = plt.axes(projection=self.proj)
        self.transform = crs.PlateCarree()
        self.ax.coastlines(self.res, linewidth=self.linewidth, color=self.color)
        self.ax.add_feature(
            cartopy.feature.LAKES,
            edgecolor=self.color,
            facecolor="none",
            linewidth=self.linewidth,
        )
        if isinstance(self.extent, (list, tuple, set)):
            if len(self.extent) == 4:
                self.ax.set_extent(self.extent, crs=self.transform)


class RotPole(MapCreator):
    """Class for Map objects on a Regionla Map (Rotated Pole)."""

    earth_radius = 6370000.0  # Earth radius in [m]

    def __init__(self, rlon, rlat, rot_pole, **kwargs):
        """Instanciate a map with a rotated pole using Cartopy.

        Parameters:
        ==========
            rlon (xr.DataArray, np.array):
                the Longitude Vector
            rlat (xr.DataArray, np.array):
                the Latitude Vector
            rot_pole (dict):
                Dictionary containing information on the rotated pole e.g.
                {grid_north_pole_longitude: 180, grid_north_pole_latitude: 50}
            kwargs:
                Additional keyword arguments that are passed to the
                map creator factory
        """
        super().__init__(**kwargs)
        self.rlon = rlon
        self.rlat = rlat
        self.pole = (
            rot_pole["grid_north_pole_longitude"],
            rot_pole["grid_north_pole_latitude"],
        )
        self.proj = crs.RotatedPole(
            pole_longitude=self.pole[0],
            pole_latitude=self.pole[-1],
            globe=crs.Globe(
                semimajor_axis=self.earth_radius, semiminor_axis=self.earth_radius
            ),
        )
        self.ax = plt.axes(projection=self.proj)
        self.transform = self.proj
        self.ax.coastlines(self.res, linewidth=self.linewidth, color=self.color)
        self.ax.add_feature(
            cartopy.feature.LAKES,
            edgecolor=self.color,
            facecolor="none",
            linewidth=self.linewidth,
        )
        xs, ys = np.r_[min(rlon), max(rlon)], np.r_[min(rlat), max(rlat)]
        if self.extent:
            if len(self.extent) == 4:
                xs, ys, zs = self.proj.transform_points(
                    self.proj,
                    np.r_[self.extent[0], self.extent[1]],
                    np.r_[self.extent[2], self.extent[3]],
                ).T
        self.ax.set_xlim(xs)
        self.ax.set_ylim(ys)


class Plotter:
    """General Plotter Class providing various plot methods."""

    tz = "utc"
    dirname = "."

    def __init__(self, mapobj, lon, lat, dset, **kwargs):
        """Instanciate the Plot object.

        Parameters:
        ===========
            mapobj: (MapCreator)
                MapCreator object where the data gets plotted onto
            lon (xr.DataArray, numpy.array):
                the Longitude Vector
            lat (xr.DataArray, numpy.array):
                the Latitude Vector
            dset (xr.DataArray, numpy.array):
                The data to be plotted
        """
        try:
            long_name = dset.attrs["long_name"]
            units = dset.attrs["units"]
        except (KeyError, AttributeError):
            long_name = units = ""
        cbar_label = f"{long_name} [{units}]"
        self.extent = kwargs.pop("lonlatbox", None)
        for key, v in dict(fontsize=14, cbar_label=cbar_label, title="").items():
            mapobj._kwargs.setdefault(key, v)
        self.proj = mapobj.proj
        self.lon = lon
        self.lat = lat
        self.cbar = None
        self.im = None
        self.fig = mapobj.fig
        self.ax = mapobj.ax
        self.transform = mapobj.transform
        self._cbar_label = mapobj._kwargs.pop("cbar_label")
        self.fontsize = mapobj._kwargs.pop("fontsize")
        self.title = mapobj._kwargs.pop("title")
        self.ax.set_title(self.title, fontsize=self.fontsize)
        self._kwargs = mapobj._kwargs

    @classmethod
    def from_rotated_grid(cls, rlon, rlat, dset, rot_pole, *args, **kwargs):
        """Create a Plot object for plotting on a map with a rotated pole.
        Parameters:
        ==========
            rlon (xr.DataArray, np.array):
                the Longitude Vector
            rlat (xr.DataArray, np.array):
                the Latitude Vector
            dset (xr.DataArray, numpy.array):
                The data to be plotted
            rot_pole (dict):
                Dictionary containing information on the rotated pole e.g.
                {grid_north_pole_longitude: 180, grid_north_pole_latitude: 50}
            kwargs:
                Additional keyword arguments that are passed to the
                map creator factory

        Returns:
        ========
        Plotter: Instance of the Plot obj for plotting on maps with roated poles
        """

        Map = RotPole(rlon, rlat, rot_pole, **kwargs)
        cls.tz = "Europe/Berlin"
        return cls(Map, rlon, rlat, dset, **kwargs)

    @classmethod
    def from_global_grid(cls, lon, lat, dset, map_call, *args, **kwargs):
        """Create a Plot object for plotting on a global map.

        Parameters:
        ===========
            lon (xr.DataArray, np.array):
                the Longitude Vector
            lat (xr.DataArray, np.array):
                the Latitude Vector
            dset (xr.DataArray, numpy.array):
                The data to be plotted
            map_call (str, cartopy.crs):
                The cartopy map projection, if proj is of type str then
                the projection is going to be created using the evaluation
                proj string and the cartopy.crs call
            kwargs:
                Additional keyword arguments that are passed to the
                map creator factory

        Returns:
        ========
        Plotter: Instance of the Plot obj for plotting on global maps
        """

        Map = GlobalMap(lon, lat, map_call, **kwargs)
        cls.tz = "UTC"
        return cls(Map, lon, lat, dset, **kwargs)

    def pcolormesh(self, data, **kwargs):
        """Plot data on a colormesh.

        Parameters:
        ===========
            data : (xr.DataArray, numpy.array)
                2D Data to be plotted
        """
        # kwargs.setdefault('shading', 'nearest')
        # kwargs.setdefault('add_colorbar', False)
        kwargs.setdefault("antialiased", True)
        kwargs.setdefault("transform", self.transform)
        try:
            kwargs.pop("lonlatbox")
        except KeyError:
            pass
        try:
            plot_data = data.values
        except AttributeError:
            plot_data = data
        im = plt.pcolormesh(self.lon, self.lat, plot_data, **kwargs)
        self.ax.set_title(self.title, fontsize=self.fontsize)
        if self.cbar is None:
            cbar_label = self.get_cbar_label(data)
            self.cbar = self.fig.colorbar(
                im,
                ax=self.ax,
                orientation="horizontal",
                shrink=0.74,
                aspect=40,
                pad=0.02,
                extend="both",
            )
            self.cbar.set_label(cbar_label, fontsize=self.fontsize)
        return im

    @staticmethod
    def get_timestamp(dset, fmt=" (%Y/%m/%d %H:%M)", tz="utc"):
        """Construt the string repr. of a timestep from a given xr.DataArray."""
        if dset.time.values is None or dset.time.values == np.array(None):
            return ""
        try:
            time = pd.DatetimeIndex(
                np.array([dset.time.data]), tz="utc"
            ).to_pydatetime()[0]
        except AttributeError:
            return ""
        except TypeError:
            units = "seconds since 1970-01-01:00:00"
            try:
                tt = cftime.date2num(dset.time.values, units)
            except:
                return ""
            return cftime.num2date(tt, units).strftime(fmt)
        except ValueError:
            return ""
        time_lt = time.astimezone(pytz.timezone(tz))
        return time_lt.strftime(fmt)

    def get_cbar_label(self, dset):
        """Construct the label of the colorbar."""
        return self._cbar_label + self.get_timestamp(dset, tz=self.tz)


def plot_map(dset, lon, lat, *args, plot=None, dirname=None, **kwargs):
    """Create a plot of an xarray.DtaArray.

    Parameters:
    ===========
        lon (xr.DataArray, np.array):
                the Longitude Vector
        lat (xr.DataArray, np.array):
                the Latitude Vector
        dset (xr.DataArray):
                The data to be plotted
        plot (Plotter):
            Instance of a plotter_obj, if none given (default) a new object
            will be created.
        dirname (str, pathlib.Path):
            The filename of the plot. If you set this to None (default)
            now figure will be saved to file, but rather a plot object
            will be returned for addtional work.
        kwargs: Additional keyword arguments for the Plotting and Mapping
                classes
    """

    map_method = kwargs.pop("mapping_method", "from_global_grid")
    close_fig = False
    if plot is None:
        close_fig = True
        plot = getattr(Plotter, map_method)(lon, lat, dset, *args, **kwargs)
    _ = dset.plot(
        ax=plot.ax,
        transform=plot.transform,
        add_colorbar=True,
        cbar_kwargs=dict(
            label=plot.get_cbar_label(dset),
            orientation="horizontal",
            shrink=0.74,
            aspect=40,
            pad=0.02,
            extend="both",
        ),
        **plot._kwargs,
    )
    # plot.im = getattr(plot, plot_method)(dset, **plot._kwargs)
    plot.ax.set_title(plot.title or "", fontsize=plot.fontsize)
    if not dirname:
        return plot
    ts = plot.get_timestamp(dset, fmt="_%Y%m%d_%H%M", tz=plot.tz)
    fn = f"{dset.name}"
    if ts:
        fn += ts
    fn += ".png"
    dirname = Path(dirname)
    fname = (dirname / fn).expanduser().absolute()
    plot.fig.savefig(str(fname), dpi=150, bbox_inches="tight")
    if close_fig:
        plot.fig.clf()
        plt.close()
    return fname


def _pool_wrapper(first_arg, func=plot_map, args=(), kwargs={}):
    """Wrapper to apply a given function in a mp.Pool."""

    return func(first_arg, *args, **kwargs)
